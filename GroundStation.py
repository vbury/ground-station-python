from dronekit import *
from serial import *
import time

# Connect to the Vehicle (in this case a simulator running the same computer)
vehicle = connect('tcp:127.0.0.1:5760', wait_ready=True)
# vehicle = connect('10.0.0.2:6000', wait_ready=True)
# vehicle = connect('/dev/cu.usbserial-DN012VYQ',baud=57600, wait_ready=True)


def send_ned_velocity(velocity_x, velocity_y, velocity_z, duration):
    """
    Move vehicle in direction based on specified velocity vectors.
    """
    msg = vehicle.message_factory.set_position_target_local_ned_encode(
        0,       # time_boot_ms (not used)
        0, 0,    # target system, target component
        mavutil.mavlink.MAV_FRAME_LOCAL_NED,  # frame
        0b0000111111000111,  # type_mask (only speeds enabled)
        0, 0, 0,  # x, y, z positions (not used)
        velocity_x, velocity_y, velocity_z,  # x, y, z velocity in m/s
        # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
        0, 0, 0,
        0, 0)    # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)

    # send command to vehicle on 1 Hz cycle
    for x in range(0, duration):
        vehicle.send_mavlink(msg)
        time.sleep(1)


def get_distance_metres(aLocation1, aLocation2):
    """
    Returns the ground distance in metres between two LocationGlobal objects.

    This method is an approximation, and will not be accurate over large distances and close to the
    earth's poles. It comes from the ArduPilot test code:
    https://github.com/diydrones/ardupilot/blob/master/Tools/autotest/common.py
    """
    dlat = aLocation2.lat - aLocation1.lat
    dlong = aLocation2.lon - aLocation1.lon
    return math.sqrt((dlat * dlat) + (dlong * dlong)) * 1.113195e5

print "Welcome"
print "This is an autopilot control program for ardupilot"
print "Version: 1.0.0"
print "Vehicle location: ", vehicle.location.global_frame
print "Relative vehicle location: ", vehicle.location.global_relative_frame
print "   -----------------------------"
print "K -> Keyboard control"
print "M -> Mission control"
print "I -> Vehicle state"
print "L -> Emergency land"
print "H -> Emergency home"
print "E -> Emergency stop"
print "Q -> Quit"
menu = raw_input("Do your choice: ")
if menu.lower() == 'i':
    print "   -----------------------------"
    print "Vehicle state init"
    while True:
        # Get all vehicle attributes (state)
        print "   -----------------------------"
        print "\nGet all vehicle attribute values:"
        print " Autopilot Firmware version: %s" % vehicle.version
        print "   Major version number: %s" % vehicle.version.major
        print "   Minor version number: %s" % vehicle.version.minor
        print "   Patch version number: %s" % vehicle.version.patch
        print "   Release type: %s" % vehicle.version.release_type()
        print "   Release version: %s" % vehicle.version.release_version()
        print "   Stable release?: %s" % vehicle.version.is_stable()
        print " Autopilot capabilities"
        print "   Supports MISSION_FLOAT message type: %s" % vehicle.capabilities.mission_float
        print "   Supports PARAM_FLOAT message type: %s" % vehicle.capabilities.param_float
        print "   Supports MISSION_INT message type: %s" % vehicle.capabilities.mission_int
        print "   Supports COMMAND_INT message type: %s" % vehicle.capabilities.command_int
        print "   Supports PARAM_UNION message type: %s" % vehicle.capabilities.param_union
        print "   Supports ftp for file transfers: %s" % vehicle.capabilities.ftp
        print "   Supports commanding attitude offboard: %s" % vehicle.capabilities.set_attitude_target
        print "   Supports commanding position and velocity targets in local NED frame: %s" % vehicle.capabilities.set_attitude_target_local_ned
        print "   Supports set position + velocity targets in global scaled integers: %s" % vehicle.capabilities.set_altitude_target_global_int
        print "   Supports terrain protocol / data handling: %s" % vehicle.capabilities.terrain
        print "   Supports direct actuator control: %s" % vehicle.capabilities.set_actuator_target
        print "   Supports the flight termination command: %s" % vehicle.capabilities.flight_termination
        print "   Supports mission_float message type: %s" % vehicle.capabilities.mission_float
        print "   Supports onboard compass calibration: %s" % vehicle.capabilities.compass_calibration
        print " Global Location: %s" % vehicle.location.global_frame
        print " Global Location (relative altitude): %s" % vehicle.location.global_relative_frame
        print " Local Location: %s" % vehicle.location.local_frame
        print " Attitude: %s" % vehicle.attitude
        print " Velocity: %s" % vehicle.velocity
        print " GPS: %s" % vehicle.gps_0
        print " Gimbal status: %s" % vehicle.gimbal
        print " Battery: %s" % vehicle.battery
        print " EKF OK?: %s" % vehicle.ekf_ok
        print " Last Heartbeat: %s" % vehicle.last_heartbeat
        print " Rangefinder: %s" % vehicle.rangefinder
        print " Rangefinder distance: %s" % vehicle.rangefinder.distance
        print " Rangefinder voltage: %s" % vehicle.rangefinder.voltage
        print " Heading: %s" % vehicle.heading
        print " Is Armable?: %s" % vehicle.is_armable
        print " System status: %s" % vehicle.system_status.state
        print " Groundspeed: %s" % vehicle.groundspeed    # settable
        print " Airspeed: %s" % vehicle.airspeed    # settable
        print " Mode: %s" % vehicle.mode.name    # settable
        print " Armed: %s" % vehicle.armed    # settable
        print "   -----------------------------"
        print "K -> Keyboard control"
        print "M -> Mission control"
        print "I -> Vehicle state"
        print "Q -> Quit"
        menu = raw_input("Do your choice: ")
        if menu.lower() == "i":
            print "   -----------------------------"
            print "Vehicle state init"
        elif menu.lower() == "m":
            print "   -----------------------------"
            print "Mission control init"
            menu = "m"
            break
        elif menu.lower() == "k":
            print "   -----------------------------"
            print "Keyboard control init"
            menu = "k"
            break
        elif menu.lower() == "q":
            print "   -----------------------------"
            print "Good Bye"
            sys.exit()
        else:
            print "   -----------------------------"
            print "An unexpected error happened"
            sys.exit()
if menu.lower() == "l":
    print "Taking Down!"
    vehicle.mode = VehicleMode("LAND")
    while vehicle.location.global_relative_frame.alt >= 0:
        print " GPS: ", vehicle.location.global_relative_frame
        time.sleep(1)
    vehicle.close()
    sys.exit()

if menu.lower() == "h":
    print "Taking Home!"
    vehicle.mode = VehicleMode("RTL")
    while vehicle.location.global_relative_frame.alt >= 0:
        print " GPS: ", vehicle.location.global_relative_frame
        time.sleep(1)
    vehicle.close()
    sys.exit()

if menu.lower() == "e":
    print "ALL Stop!"
    vehicle.armed = False
    while vehicle.location.global_relative_frame.alt >= 0:
        print " GPS: ", vehicle.location.global_relative_frame
        time.sleep(1)
    vehicle.close()
    sys.exit()

if menu.lower() == "k":
    print "Vehicle location: ", vehicle.location.global_frame
    if vehicle.location.global_frame.alt > 1 or vehicle.location.global_frame.alt == None:
        print "Altitude = ", vehicle.location.global_frame.alt
        print "Do you want to continue ?"
        while True:
            print "C -> continue"
            print "G -> GPS"
            print "Q -> quit"
            LocValue = raw_input('Do your choice: ')
            if LocValue.lower() == 'c':
                print "Be carefull, you can easily break your drone"
                break
            elif LocValue.lower() == 'g':
                print "Vehicle location: ", vehicle.location.global_frame
            elif LocValue.lower() == 'q':
                print "Good Bye"
                sys.exit()
            else:
                print "An unexpected error happened"

    print "Do you want to continue ? y/n"
    validate = raw_input()
    if validate.lower() == "y":
        aTargetAltitude = input('Enter altitude : ')
        if aTargetAltitude < 10:
            print "Basic pre-arm checks"
            # Don't try to arm until autopilot is ready
            while not vehicle.is_armable:
                print " Waiting for vehicle to initialise..."
                time.sleep(1)
            print "Vehicle location: ", vehicle.location.global_frame
            print "Relative vehicle location: ", vehicle.location.global_relative_frame
            print "Do you want to set home here ? y/n"
            sethome = raw_input()
            if sethome == "y":
                vehicle.home_location = vehicle.location.global_frame
                print "Home: ", vehicle.home_location
                print "Vehicle location: ", vehicle.location.global_frame
                val = get_distance_metres(vehicle.home_location,
                                          vehicle.location.global_frame)
                if val != 0:
                    print "An unexpected error happened"
                    sys.exit()

            print "Drone arming"
            print "Do you want to continue ? y/n"
            validate = raw_input()
            if validate.lower() == "y":
                print "Arming motors"
                vehicle.mode = VehicleMode("GUIDED")
                vehicle.armed = True

                loc = vehicle.location.global_relative_frame

                # Confirm vehicle armed before attempting to take off
                while not vehicle.armed:
                    print " Waiting for arming..."
                    print " Vehicle location: ", vehicle.location.global_frame
                    print " If you have detected an anomaly stop the program now !"
                    time.sleep(1)

                print "Taking off!"
                # Take off to target altitude
                vehicle.simple_takeoff(aTargetAltitude)

                # Wait until the vehicle reaches a safe height before processing the goto (otherwise the command
                #  after Vehicle.simple_takeoff will execute immediately).
                while vehicle.location.global_relative_frame.alt <= aTargetAltitude * 0.95:
                    print " Altitude: ", vehicle.location.global_relative_frame.alt
                    print "Vehicle location: ", vehicle.location.global_frame
                    time.sleep(1)

                # Set up velocity mappings
                # velocity_x > 0 => fly North
                # velocity_x < 0 => fly South
                # velocity_y > 0 => fly East
                # velocity_y < 0 => fly West
                # velocity_z < 0 => ascend
                # velocity_z > 0 => descend

                while True:
                    print "Z -> fly North"
                    print "S -> fly South"
                    print "D -> fly East"
                    print "Q -> fly West"
                    print "T -> fly Up"
                    print "G -> fly Down"
                    print "X -> GPS"
                    print "C -> Direction"
                    print "V -> Info Direction"
                    print "H -> fly Home"
                    print "L -> fly Land"
                    #print "E -> Stop Motor"
                    value = raw_input('Go : ')
                    if value.lower() == 'z':
                        send_ned_velocity(0.32, 0, 0, 1)
                        print "Fly North"
                        time.sleep(3)
                        print " GPS: ", vehicle.location.global_relative_frame
                    elif value.lower() == 's':
                        send_ned_velocity(-0.32, 0, 0, 1)
                        print "Fly South"
                        time.sleep(3)
                        print " GPS: ", vehicle.location.global_relative_frame
                    elif value.lower() == 'd':
                        send_ned_velocity(0, 0.32, 0, 1)
                        print "Fly East"
                        time.sleep(3)
                        print " GPS: ", vehicle.location.global_relative_frame
                    elif value.lower() == 'q':
                        send_ned_velocity(0, -0.32, 0, 1)
                        print "Fly West"
                        time.sleep(3)
                        print " GPS: ", vehicle.location.global_relative_frame
                    elif value.lower() == 't':
                        send_ned_velocity(0, 0, -0.32, 1)
                        print "Fly Up"
                        time.sleep(3)
                        print " GPS: ", vehicle.location.global_relative_frame
                    elif value.lower() == 'g':
                        send_ned_velocity(0, 0, 0.32, 1)
                        print "Fly Down"
                        time.sleep(3)
                        print " GPS: ", vehicle.location.global_relative_frame
                    elif value.lower() == 'c':
                        print "Yaw: ", vehicle.attitude.yaw
                        degre = input('Entrer in degrees: ')
                        if degre <= 360:
                            vehicle.gimbal.rotate(0, 0, degre)
                        else:
                            print "Bad value"
                        time.sleep(10)
                        print "Yaw after: ", vehicle.attitude.yaw
                    elif value.lower() == 'b':
                        print "Attitude: ", vehicle.attitude
                        vehicle.gimbal.target_location(vehicle.home_location)
                        time.sleep(10)
                    elif value.lower() == 'v':
                        print "Yaw: ", vehicle.attitude.yaw
                    elif value.lower() == 'l':
                        print "Taking Down!"
                        vehicle.mode = VehicleMode("LAND")
                        while vehicle.location.global_relative_frame.alt >= 0:
                            print " GPS: ", vehicle.location.global_relative_frame
                            print " Airspeed: ", vehicle.airspeed
                            print " Groundspeed: ", vehicle.groundspeed
                            time.sleep(1)
                        break
                    elif value.lower() == 'h':
                        print "Go Home!"
                        vehicle.mode = VehicleMode("RTL")
                        while vehicle.location.global_relative_frame.alt >= 0:
                            print " GPS: ", vehicle.location.global_relative_frame
                            time.sleep(1)
                        break
                    elif value.lower() == 'x':
                        print " GPS: ", vehicle.location.global_relative_frame
                    else:
                        print "Bad syntax"
                    val = get_distance_metres(
                        loc, vehicle.location.global_relative_frame)
                    print " Distance: ", val

                # Disarm the vehicle
                vehicle.armed = False

                while vehicle.armed:
                    print " Waiting for disarming..."
                    time.sleep(1)
            else:
                vehicle.close()
                sys.exit()
        else:
            print "To High"
            vehicle.armed = False
            while vehicle.armed:
                print " Waiting for disarming..."
                time.sleep(1)
            vehicle.close()
            sys.exit()
    else:
        vehicle.close()
        sys.exit()

vehicle.close()
