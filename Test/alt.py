from dronekit import *
from serial import *
import time

# Connect to the Vehicle (in this case a simulator running the same computer)
vehicle = connect('tcp:127.0.0.1:5760', wait_ready=True)

while True:
    print " GPS: ", vehicle.location.global_relative_frame
    time.sleep(1)