from dronekit import *
from serial import *
import time

vehicle = connect('tcp:127.0.0.1:5760', wait_ready=True)

print "\nPrint all parameters (iterate `vehicle.parameters`):"
for key, value in vehicle.parameters.iteritems():
    print " Key:%s Value:%s" % (key,value)
