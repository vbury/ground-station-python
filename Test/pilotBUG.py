from dronekit import *
from serial import *
import time

global isWindows

isWindows = False
try:
    from win32api import STD_INPUT_HANDLE
    from win32console import GetStdHandle, KEY_EVENT, ENABLE_ECHO_INPUT, ENABLE_LINE_INPUT, ENABLE_PROCESSED_INPUT
    isWindows = True
except ImportError as e:
    import sys
    import select
    import termios


class KeyPoller():
    def __enter__(self):
        global isWindows
        if isWindows:
            self.readHandle = GetStdHandle(STD_INPUT_HANDLE)
            self.readHandle.SetConsoleMode(ENABLE_LINE_INPUT|ENABLE_ECHO_INPUT|ENABLE_PROCESSED_INPUT)

            self.curEventLength = 0
            self.curKeysLength = 0

            self.capturedChars = []
        else:
            # Save the terminal settings
            self.fd = sys.stdin.fileno()
            self.new_term = termios.tcgetattr(self.fd)
            self.old_term = termios.tcgetattr(self.fd)

            # New terminal setting unbuffered
            self.new_term[3] = (self.new_term[3] & ~termios.ICANON & ~termios.ECHO)
            termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.new_term)

        return self

    def __exit__(self, type, value, traceback):
        if isWindows:
            pass
        else:
            termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.old_term)

    def poll(self):
        if isWindows:
            if not len(self.capturedChars) == 0:
                return self.capturedChars.pop(0)

            eventsPeek = self.readHandle.PeekConsoleInput(10000)

            if len(eventsPeek) == 0:
                return None

            if not len(eventsPeek) == self.curEventLength:
                for curEvent in eventsPeek[self.curEventLength:]:
                    if curEvent.EventType == KEY_EVENT:
                        if ord(curEvent.Char) == 0 or not curEvent.KeyDown:
                            pass
                        else:
                            curChar = str(curEvent.Char)
                            self.capturedChars.append(curChar)
                self.curEventLength = len(eventsPeek)

            if not len(self.capturedChars) == 0:
                return self.capturedChars.pop(0)
            else:
                return None
        else:
            dr,dw,de = select.select([sys.stdin], [], [], 0)
            if not dr == []:
                return sys.stdin.read(1)
            return None

# Connect to the Vehicle (in this case a simulator running the same computer)
vehicle = connect('tcp:127.0.0.1:5760', wait_ready=True)
#vehicle = connect('/dev/cu.usbserial-DN02A00O',baud=57600, wait_ready=True, heartbeat_timeout=60)

def send_ned_velocity(velocity_x, velocity_y, velocity_z, duration):
    """
    Move vehicle in direction based on specified velocity vectors.
    """
    msg = vehicle.message_factory.set_position_target_local_ned_encode(
        0,       # time_boot_ms (not used)
        0, 0,    # target system, target component
        mavutil.mavlink.MAV_FRAME_LOCAL_NED, # frame
        0b0000111111000111, # type_mask (only speeds enabled)
        0, 0, 0, # x, y, z positions (not used)
        velocity_x, velocity_y, velocity_z, # x, y, z velocity in m/s
        0, 0, 0, # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
        0, 0)    # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)


    # send command to vehicle on 1 Hz cycle
    for x in range(0,duration):
        vehicle.send_mavlink(msg)
        time.sleep(1)

def get_distance_metres(aLocation1, aLocation2):
    """
    Returns the ground distance in metres between two LocationGlobal objects.

    This method is an approximation, and will not be accurate over large distances and close to the 
    earth's poles. It comes from the ArduPilot test code: 
    https://github.com/diydrones/ardupilot/blob/master/Tools/autotest/common.py
    """
    dlat = aLocation2.lat - aLocation1.lat
    dlong = aLocation2.lon - aLocation1.lon
    return math.sqrt((dlat*dlat) + (dlong*dlong)) * 1.113195e5

aTargetAltitude=input('Enter altitude : ')
if aTargetAltitude<10:
    print "Basic pre-arm checks"
        # Don't try to arm until autopilot is ready
    while not vehicle.is_armable:
        print " Waiting for vehicle to initialise..."
        time.sleep(1)

    print "Arming motors"
    # Copter should arm in GUIDED mode
    vehicle.home_location=vehicle.location.global_frame
    print "Home: ", vehicle.home_location
    print "Vehicle location: ", vehicle.location.global_frame
    val=get_distance_metres(vehicle.home_location, vehicle.location.global_frame)
    if val != 0:
        sys.exit()
    vehicle.mode    = VehicleMode("GUIDED")
    vehicle.armed   = True

    loc=vehicle.location.global_relative_frame

    # Confirm vehicle armed before attempting to take off
    while not vehicle.armed:
        print " Waiting for arming..."
        time.sleep(1)

    print "Taking off!"
    vehicle.simple_takeoff(aTargetAltitude) # Take off to target altitude
else:
    print "To High"
    vehicle.armed   = False
    while vehicle.armed:
        print " Waiting for disarming..."
        time.sleep(1)
    vehicle.close()
    sys.exit()

# Wait until the vehicle reaches a safe height before processing the goto (otherwise the command
#  after Vehicle.simple_takeoff will execute immediately).
while vehicle.location.global_relative_frame.alt<=aTargetAltitude*0.95:
    print " Altitude: ", vehicle.location.global_relative_frame.alt
    time.sleep(1)

# Set up velocity mappings
# velocity_x > 0 => fly North
# velocity_x < 0 => fly South
# velocity_y > 0 => fly East
# velocity_y < 0 => fly West
# velocity_z < 0 => ascend
# velocity_z > 0 => descend
print "THIS PROGRAM IS EXPERIMENTAL"
print "BE CAREFULL WITH REAL DRONE"
with KeyPoller() as keyPoller:
    print "Z -> fly North"
    print "S -> fly South"
    print "D -> fly East"
    print "Q -> fly West"
    print "T -> fly Up"
    print "G -> fly Down"
    print "X -> GPS"
    print "C -> Direction"
    print "V -> Info Direction"
    print "H -> fly Home"
    print "L -> fly Land"
    while True:
        value = keyPoller.poll()
        time.sleep(1)
        if not value is None:
            if value.lower() == 'z':
                send_ned_velocity(0.32,0,0,1)
                print "Fly North"
                print " GPS: ", vehicle.location.global_relative_frame
            elif value.lower() == 's':
                send_ned_velocity(-0.32,0,0,1)
                print "Fly South"
                print " GPS: ", vehicle.location.global_relative_frame
            elif value.lower() == 'd':
                send_ned_velocity(0,0.32,0,1)
                print "Fly East"
                print " GPS: ", vehicle.location.global_relative_frame
            elif value.lower() == 'q':
                send_ned_velocity(0,-0.32,0,1)
                print "Fly West"
                print " GPS: ", vehicle.location.global_relative_frame
            elif value.lower() == 't':
                send_ned_velocity(0,0,-0.32,1)
                print "Fly Up"
                print " GPS: ", vehicle.location.global_relative_frame
            elif value.lower() == 'g':
                send_ned_velocity(0,0,0.32,1)
                print "Fly Down"
                print " GPS: ", vehicle.location.global_relative_frame
            elif value.lower() == 'c':
                print "Yaw: ", vehicle.attitude.yaw
                degre=input('Entrer in degrees: ')
                if degre<=360:
                    vehicle.gimbal.rotate(0, 0, degre)
                else:
                    print "Bad value"
                time.sleep(10)
                print "Yaw after: ", vehicle.attitude.yaw
            elif value.lower() == 'b':
                print "Attitude: ", vehicle.attitude
                vehicle.gimbal.target_location(vehicle.home_location)
                time.sleep(10)
            elif value.lower() == 'v':
                print "Yaw: ", vehicle.attitude.yaw
            elif value.lower() == 'l':
                print "Taking Down!"
                vehicle.mode    = VehicleMode("LAND")
                while vehicle.location.global_relative_frame.alt>=0:
                    print " GPS: ", vehicle.location.global_relative_frame
                    time.sleep(1)
                break
            elif value.lower() == 'h':
                print "Go Home!"
                vehicle.mode    = VehicleMode("RTL")
                while vehicle.location.global_relative_frame.alt>=0:
                    print " GPS: ", vehicle.location.global_relative_frame
                    time.sleep(1)
                break
            elif value.lower()== 'x':
                print " GPS: ", vehicle.location.global_relative_frame
            else:
                print "Bad syntax"
            val=get_distance_metres(loc,vehicle.location.global_relative_frame)
            print " Distance: ", val
    
# Disarm the vehicle
vehicle.armed = False

while vehicle.armed:
    print " Waiting for disarming..."
    time.sleep(1)
    
vehicle.close()