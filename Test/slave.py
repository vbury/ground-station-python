from dronekit import *
from serial import *
import time

# Connect to the Vehicle (in this case a UDP endpoint)
vehicle = connect('11.0.0.1:6000', wait_ready=True)
vehicle2 = connect('11.0.0.1:6001', wait_ready=True)
#vehicle = connect('/dev/ttyUSB0', wait_ready=True)

print "\nSet Vehicle.armed=True (currently: %s)" % vehicle.armed 
#vehicle.mode    = VehicleMode("GUIDED")
vehicle.armed = True
vehicle2.armed = True

while not vehicle.armed:
    print " Waiting for arming..."
    time.sleep(1)
print " Vehicle is armed: %s" % vehicle.armed 
#print " Vehicle2 is armed: %s" % vehicle2.armed 

print("Completed")