from dronekit import *
from serial import *
import time

vehicle = connect('tcp:127.0.0.1:5760', wait_ready=True)

print "\nSet Vehicle.armed=True (currently: %s)" % vehicle.armed 
vehicle.armed = True
while not vehicle.armed:
    print " Waiting for arming..."
    time.sleep(1)
print " Vehicle is armed: %s" % vehicle.armed 
time.sleep(10)

vehicle.close()
print("Completed")