from dronekit import *
from serial import *
import time

vehicle = connect('127.0.0.1:6001', wait_ready=True)

#Test master - slave
while True:
    if vehicle.armed:
        vehicle2 = connect('11.0.0.1:6000', wait_ready=True)
        vehicle3 = connect('11.0.0.1:6001', wait_ready=True)
        print "\nSet Vehicle.armed=True (currently: %s)" % vehicle.armed 
        vehicle2.armed = True
        vehicle3.armed = True
        while not vehicle2.armed and vehicle3.armed:
            print " Waiting for arming..."
            time.sleep(1)
        print " Vehicle2 is armed: %s" % vehicle2.armed 
        print " Vehicle3 is armed: %s" % vehicle3.armed 
        break
    else:
        print " Vehicle is armed: %s" % vehicle.armed

print("Completed")
