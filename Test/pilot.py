from dronekit import *
from serial import *
import time

# Pilot on keyboard


class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen."""

    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:

    def __init__(self):
        import tty
        import sys

    def __call__(self):
        import sys
        import tty
        import termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:

    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


getch = _Getch()


def send_ned_velocity(velocity_x, velocity_y, velocity_z, duration):
    """
    Move vehicle in direction based on specified velocity vectors.
    """
    msg = vehicle.message_factory.set_position_target_local_ned_encode(
        0,       # time_boot_ms (not used)
        0, 0,    # target system, target component
        mavutil.mavlink.MAV_FRAME_LOCAL_NED,  # frame
        0b0000111111000111,  # type_mask (only speeds enabled)
        0, 0, 0,  # x, y, z positions (not used)
        velocity_x, velocity_y, velocity_z,  # x, y, z velocity in m/s
        # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
        0, 0, 0,
        0, 0)    # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)

    # send command to vehicle on 1 Hz cycle
    for x in range(0, duration):
        vehicle.send_mavlink(msg)
        time.sleep(1)


def get_distance_metres(aLocation1, aLocation2):
    """
    Returns the ground distance in metres between two LocationGlobal objects.

    This method is an approximation, and will not be accurate over large distances and close to the 
    earth's poles. It comes from the ArduPilot test code: 
    https://github.com/diydrones/ardupilot/blob/master/Tools/autotest/common.py
    """
    dlat = aLocation2.lat - aLocation1.lat
    dlong = aLocation2.lon - aLocation1.lon
    return math.sqrt((dlat * dlat) + (dlong * dlong)) * 1.113195e5

vehicle = connect('tcp:127.0.0.1:5760', wait_ready=True)

aTargetAltitude = input('Enter altitude : ')
if aTargetAltitude < 10:
    print "Basic pre-arm checks"
    # Don't try to arm until autopilot is ready
    while not vehicle.is_armable:
        print " Waiting for vehicle to initialise..."
        time.sleep(1)

    print "Arming motors"
    # Copter should arm in GUIDED mode
    vehicle.home_location = vehicle.location.global_frame
    print "Home: ", vehicle.home_location
    print "Vehicle location: ", vehicle.location.global_frame
    val = get_distance_metres(vehicle.home_location,
                              vehicle.location.global_frame)
    if val != 0:
        sys.exit()
    vehicle.mode = VehicleMode("GUIDED")
    vehicle.armed = True

    loc = vehicle.location.global_relative_frame

    # Confirm vehicle armed before attempting to take off
    while not vehicle.armed:
        print " Waiting for arming..."
        time.sleep(1)

    print "Taking off!"
    vehicle.simple_takeoff(aTargetAltitude)  # Take off to target altitude
else:
    print "To High"
    vehicle.armed = False
    while vehicle.armed:
        print " Waiting for disarming..."
        time.sleep(1)
    vehicle.close()
    sys.exit()

# Wait until the vehicle reaches a safe height before processing the goto (otherwise the command
#  after Vehicle.simple_takeoff will execute immediately).
while vehicle.location.global_relative_frame.alt <= aTargetAltitude * 0.95:
    print " Altitude: ", vehicle.location.global_relative_frame.alt
    time.sleep(1)

# Set up velocity mappings
# velocity_x > 0 => fly North
# velocity_x < 0 => fly South
# velocity_y > 0 => fly East
# velocity_y < 0 => fly West
# velocity_z < 0 => ascend
# velocity_z > 0 => descend
print "THIS PROGRAM IS EXPERIMENTAL"
print "BE CAREFULL WITH REAL DRONE"
print "Z -> fly North"
print "S -> fly South"
print "D -> fly East"
print "Q -> fly West"
print "T -> fly Up"
print "G -> fly Down"
print "X -> GPS"
print "C -> Orientation"
print "V -> Info Orientation"
print "H -> fly Home"
print "L -> fly Land"
print "ESC -> quit"
while True:
    key = ord(getch())
    if key == 122:  # Z
        send_ned_velocity(0.32, 0, 0, 1)
        print "Fly North"
        print " GPS: ", vehicle.location.global_relative_frame
    elif key == 115:  # S
        send_ned_velocity(-0.32, 0, 0, 1)
        print "Fly South"
        print " GPS: ", vehicle.location.global_relative_frame
    elif key == 100:  # D
        send_ned_velocity(0, 0.32, 0, 1)
        print "Fly East"
        print " GPS: ", vehicle.location.global_relative_frame
    elif key == 113:  # Q
        send_ned_velocity(0, -0.32, 0, 1)
        print "Fly West"
        print " GPS: ", vehicle.location.global_relative_frame
    elif key == 112:  # P
        send_ned_velocity(0, 0, -0.32, 1)
        print "Fly Up"
        print " GPS: ", vehicle.location.global_relative_frame
    elif key == 109:  # M
        send_ned_velocity(0, 0, 0.32, 1)
        print "Fly Down"
        print " GPS: ", vehicle.location.global_relative_frame
    elif key == 120:  # X
        print " GPS: ", vehicle.location.global_relative_frame
    elif key == 108:  # L
        print "Taking Down!"
        vehicle.mode = VehicleMode("LAND")
        while vehicle.location.global_relative_frame.alt >= 0:
            print " GPS: ", vehicle.location.global_relative_frame
            val = get_distance_metres(
                loc, vehicle.location.global_relative_frame)
            print " Distance: ", val
            time.sleep(1)
        break
    elif key == 99:  # C
        print "Yaw: ", vehicle.attitude.yaw
        degre = input('Entrer in degrees: ')
        if degre <= 360:
            vehicle.gimbal.rotate(0, 0, degre)
        else:
            print "Bad value"
        time.sleep(10)
        print "Yaw after: ", vehicle.attitude.yaw
    elif key == 118:  # V
        print "Yaw: ", vehicle.attitude.yaw
    elif key == 104:
        print "Go Home!"
        vehicle.mode = VehicleMode("RTL")
        while vehicle.location.global_relative_frame.alt >= 0:
            print " GPS: ", vehicle.location.global_relative_frame
            val = get_distance_metres(
                loc, vehicle.location.global_relative_frame)
            print " Distance: ", val
            time.sleep(1)
        break
    elif key == 27:  # ESC
        break
    val = get_distance_metres(loc, vehicle.location.global_relative_frame)
    print " Distance: ", val
