from dronekit import *
from serial import *
import time


vehicle = connect('/dev/cu.usbserial-DN02A00O',baud=57600, wait_ready=True, heartbeat_timeout=60)

print "Taking Down!"
vehicle.mode    = VehicleMode("LAND")
while vehicle.location.global_relative_frame.alt>=0:
    print " GPS: ", vehicle.location.global_relative_frame
    time.sleep(1)

vehicle.close()